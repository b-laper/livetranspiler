import MonacoEditor from "@monaco-editor/react";

export const CodeEditor = () => {
  return (
    <MonacoEditor
      height="500px"
      language="javascript"
      options={{
        wordWrap: "on",
      }}
      theme="vs-dark"
    />
  );
};
